#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAGICNUMBER 0xdeadbeef

typedef struct {
    int magic;
    char* directives[2];
} config_t;


config_t* readconfig(const char* filename) {
    FILE* fp = fopen(filename, "r");
    config_t* c = malloc(sizeof(config_t));

    if(fread(&c->magic, sizeof(int), 1, fp) != 1) {
        fclose(fp);
        free(c);
        return NULL;
    }

    c->directives[0] = (char*)malloc(21);
    c->directives[1] = (char*)malloc(21);
    fscanf(fp, "%20s", c->directives[0]);
    fscanf(fp, "%20s", c->directives[1]);
    fclose(fp);

    return c;
}


void initialize(config_t* config) {
    printf("initialize\n");
    return;
}


void programbug() {
    volatile char* c = NULL;
    *c = 0;
}


void setoption(char* optionstring) {
    printf("setoption to %s\n", optionstring);
    return;
}


void _default() {
    printf("_default\n");
    return;
}


int main(int argc, char* argv[]) {

    if(argc < 2) {
        return 0;
    }

  config_t* config = readconfig(argv[1]);
  if(config == NULL){
    puts("Configuration syntax error");
    return 1;
  }
  if (config->magic != MAGICNUMBER) {
    puts("Bad magic number");
    return 2;
  }
  initialize(config);

  char* directive = config->directives[0];
  if(!strcmp(directive, "crashstring")) {
    programbug();
  }
  else if(!strcmp(directive, "setoption")) {
    setoption(config->directives[1]);
  }
  else{
    _default();
  }

  return 0;
}