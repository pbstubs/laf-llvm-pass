#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "png.h"
#include <signal.h>

int x, y;

int width, height;
png_byte color_type;
png_byte bit_depth;

png_structp png_ptr;
png_infop info_ptr;
int number_of_passes;
png_bytep * row_pointers;


void read_png_file(FILE* fp)
{
        char header[8];    // 8 is the maximum size that can be checked

        /* open file and test for it being a png */
        fread(header, 1, 8, fp);
        if (png_sig_cmp(header, 0, 8))
            return;

        /* initialize stuff */
        png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

        if (!png_ptr)
            return;

        info_ptr = png_create_info_struct(png_ptr);
        if (!info_ptr) {
            png_destroy_read_struct(&png_ptr, NULL, NULL);
            return;
        }

        if (setjmp(png_jmpbuf(png_ptr))) {
            png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
            return;
        }

        png_init_io(png_ptr, fp);
        png_set_sig_bytes(png_ptr, 8);

        png_read_info(png_ptr, info_ptr);

        width = png_get_image_width(png_ptr, info_ptr);
        height = png_get_image_height(png_ptr, info_ptr);
        color_type = png_get_color_type(png_ptr, info_ptr);
        bit_depth = png_get_bit_depth(png_ptr, info_ptr);

        number_of_passes = png_set_interlace_handling(png_ptr);
        png_read_update_info(png_ptr, info_ptr);


        /* read file */
        if (setjmp(png_jmpbuf(png_ptr))) {
            png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
            return;
        }

        row_pointers = (png_bytep*) malloc(sizeof(png_bytep) * height);
        //printf("row_pointers %p\n", row_pointers);

        if(!row_pointers) {
            png_create_info_struct(png_ptr);
            return;
        }
        for (y=0; y<height; y++) {
                uint32_t rowbytes = png_get_rowbytes(png_ptr,info_ptr);
                row_pointers[y] = (png_byte*)malloc(rowbytes);
                if(!row_pointers[y]) {
                    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
                    return;
                }
        }

        png_read_image(png_ptr, row_pointers);
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
        
}


int main(int argc, char **argv) {

    if(argc < 2) {
        printf("not enough args\n");
        return -1;
    }

    while(__AFL_LOOP(10000)) {
        FILE *fp = fopen(argv[1], "rb");

        row_pointers = NULL;
        read_png_file(fp);
        fclose(fp);

        if(row_pointers) {
            int i;
            for(i = 0; i < height; i++) {
                if(row_pointers[i]) {
                    free(row_pointers[i]);
                    row_pointers[i] = NULL;
                }
            }
            free(row_pointers);
            row_pointers = 0;
        }
    }

    return 0;
}
